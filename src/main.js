import Vue from 'vue';

import 'normalize.css/normalize.css'; // A modern alternative to CSS resets

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'; // lang i18n

import '@/styles/index.scss'; // global css

import App from './App';
import store from './store';
import router from './router';

import '@/icons'; // icon
import '@/permission'; // permission control

/**
 * This project originally used easy-mock to simulate data,
 * but its official service is very unstable,
 * and you can build your own service if you need it.
 * So here I use Mock.js for local emulation,
 * it will intercept your request, so you won't see the request in the network.
 * If you remove `../mock` it will automatically request easy-mock data.
 */
// import '../mock'; // simulation data
import { getMenu } from '@/api/menu';
import { SetPermission } from './permission';

import VueI18n from 'vue-i18n'; // 引入 Vue I18n
import tw from './VueI18n/language-tw.js'; // 存放中文語系檔
import en from './VueI18n/language-en.js'; // 存放英文語系檔

Vue.use(ElementUI, { locale });

Vue.use(VueI18n); //通过插件的形式挂载

const i18n = new VueI18n({
	locale: 'tw', // 默认语言
	messages: {
		'tw': tw,//require('./VueI18n/language-tw'), // 中文语言包
		'en': en//require('./VueI18n/language-en') // 英文语言包
	}
});

Vue.config.productionTip = false;

store.dispatch('initSocket');
store.dispatch('loadAPI');

/*---------使用语言包-----------*/
// const i18n = new VueI18n({
// 	locale: 'zh-CN', // 语言标识
// 	//this.$i18n.locale // 通过切换locale的值来实现语言切换
// 	messages: {
// 		'zh-CN': require('./VueI18n/language-zh'), // 中文语言包
// 		'en-US': require('./VueI18n/language-en') // 英文语言包
// 	}
// });

/* eslint-disable no-new */
// new Vue({
// 	el: '#app',
// 	i18n, //挂载到实例，一定得在这个位置，而不是comonents中
// 	router,
// 	components: { App },
// 	template: '<App/>'
// });

getMenu()
	.then(res => {
		return SetPermission();
	})
	.then(result => {
		new Vue({
			el: '#app',
			data: function() {
				return {
					loaded: true
				};
			},
			i18n, //挂载到实例，一定得在这个位置，而不是comonents中
			router,
			store,
			render: h => h(App)
		});
	})
	.catch(err => {
		console.error(err);
	});
