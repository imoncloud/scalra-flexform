import axios from 'axios';
import store from '@/store';

export function getConfig() {
	const url = '/api/_config';

	return axios
		.get(url)
		.then(response => {
			// debugger
			// handle success
			let config = response.data;
			store.state.config = config;
			return config;
		})
		.catch(error => {
			// handle error
			console.log(error);
		});
}
