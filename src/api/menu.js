import Create from '@/views/sf_create';
import FlowCreate from '@/views/sf_flow_create';
import FlowList from '@/views/sf_flow_list';
import FlowUser from '@/views/sf_flow_user';
import Layout from '@/views/layout/Layout';
import List from '@/views/sf_list';
import Query from '@/views/sf_query';
import DashBoard from '@/views/dashboard';
import Update from '@/views/sf_update';
import axios from 'axios';
import { permissionRoute } from '@/router';
import router from '@/router';
import store from '@/store';

export async function getMenu() {
	const menu_api_url = '/api/menu';

	const mappingComponent = function(menuStructure, level) {
		if (menuStructure.type === 'create') {
			menuStructure.component = Create;
		} else if (menuStructure.type === 'dashboard') {
			menuStructure.component = DashBoard;
		} else if (menuStructure.type === 'list') {
			menuStructure.component = List;
		} else if (menuStructure.type === 'update') {
			menuStructure.component = Update;
		} else if (menuStructure.type === 'query') {
			menuStructure.component = Query;
		} else if (menuStructure.type === 'flow_create') {
			menuStructure.component = FlowCreate;
		} else if (
			menuStructure.type === 'flow_pending' ||
			menuStructure.type === 'flow_processed' ||
			menuStructure.type === 'flow_done'
		) {
			menuStructure.component = FlowList;
		} else if (menuStructure.type === 'flow_user') {
			menuStructure.component = FlowUser;
		} else if (level === 0) {
			menuStructure.component = Layout;
		}
		if (menuStructure.hasOwnProperty('children')) {
			menuStructure.children = menuStructure.children.map(child => {
				return mappingComponent(child, level + 1);
			});
		}
		return menuStructure;
	};
	return axios
		.get(menu_api_url)
		.then(response => {
			// debugger
			// handle success
			let api_data = response.data;
			api_data = [...permissionRoute, ...api_data];
			api_data.forEach(route_data => {
				route_data = mappingComponent(route_data, 0);
				// route_data.component = Layout; // add component layout for those appears on menu
				router.addRoutes([route_data]);
			});
			store.state.routeList = api_data;
		})
		.catch(error => {
			// handle error
			console.log(error);
		})
		.then(() => {
			// always executed
		});
}
