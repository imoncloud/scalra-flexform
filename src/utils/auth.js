import Cookies from 'js-cookie';

const TokenKey = 'flexform_token';

export function getToken() {
	return Cookies.get(TokenKey);
}

export function setToken(token) {
	return Cookies.set(TokenKey, token);
}

export function removeToken() {
	Cookies.remove(TokenKey);
	Cookies.remove('roles');
	return;
}

export function setRoles(roles) {
	return Cookies.set('roles', roles.toString());
}

export function getRoles() {
	return Cookies.get('roles').split(',');
}
