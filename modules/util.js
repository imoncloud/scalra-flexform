let getMultiFormsDataDir = forms => {
	let form_names = Object.keys(forms);
	let result = [];
	for (let i in form_names) {
		let temp_form = Object.assign({}, l_get(null, form_names[i]));
		let temp_record_id = forms[form_names[i]];
		result.push({
			form_name: form_names[i],
			record_id: temp_record_id,
			values: temp_form.data.values[temp_record_id]
		});
	}
	return result;
};

let getFlowData = (flow_name, flow_record_id) => {
    let l_flow = SR.State.get('FlexFlow')
	if (
		!flow_name ||
		(flow_record_id && !l_flow[flow_name].data[flow_record_id])
	) {
		return null;
	}
	let flow_datas = l_flow[flow_name].data;
	if (!flow_record_id) {
		return flow_datas;
	} else {
		return flow_datas[flow_record_id];
	}
};

var l_get = function(id, name) {
    let l_form = SR.State.get('FlexFormMap');
	// check para availability
	if (typeof id !== 'string' && typeof name !== 'string') {
		return undefined;
	}

	var form = undefined;

	if (id && l_form.hasOwnProperty(id)) {
		form = l_form[id];
	} else if (name) {
		// search via name
		for (let form_id in l_form) {
			if (l_form[form_id].name === name) {
				form = l_form[form_id];
				break;
			}
		}
	}

	return form;
};

module.exports = {
    getMultiFormsDataDir,
    getFlowData,
}