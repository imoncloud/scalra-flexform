const express = SR.Module['express'].app;
const bodyParser = require('body-parser');
var urlParser = require('url');

express.post('/flowapi/:api_name', (req, res) => {
	console.log('呼叫flowapi2222');
	var url = urlParser.parse(req.url, true);
	var pathname = url.pathname;
	var path_array = pathname.split('/');
	let event_name = req.params['api_name'];

	let JSONobj = req.body;
	console.log(path_array);

	var cookie = SR.REST.getCookie(req.headers.cookie);

	// callback to return response to client
	var onResponse = function(res_obj, data, conn) {
		// check if we should return empty response
		if (typeof res_obj === 'undefined') {
			SR.REST.reply(res, {});
			return true;
		}

		// send back via res object if hadn't responded yet
		if (res.headersSent === false) {
			// NOTE: cookie may be undefined;
			data = JSON.parse(data);
			if (data.P.err) {
				res.status(400).send(data.P.err);
			} else {
				res.send(data.P.result);
			}
		} else {
			LOG.error(
				'HTTP request has already responded (cannot respond twice)',
				'flow_api_generator'
			);
			LOG.stack();
		}

		return true;
	};

	// build event object, also determine if coming from http or https
	// ref: http://stackoverflow.com/questions/10348906/how-to-know-if-a-request-is-http-or-https-in-node-js

	var host = req.connection.remoteAddress.split(':');
	host = host[host.length - 1];

	var from = {
		host: host,
		port: req.connection.remotePort,
		type: req.connection.encrypted ? 'HTTPS' : 'HTTP',
		cookie: cookie
	};

	var conn = SR.Conn.createConnObject(from.type, onResponse, from);
	var data = {};
	data[SR.Tags.EVENT] = event_name;
	data[SR.Tags.PARA] = JSONobj;
	var event = SR.EventManager.unpack(data, conn, from.cookie);

	// NOTE: we make path array available to the event
	path_array.splice(0, 2);
	event.conn.pathname = url.parse(req.url, true).pathname;
	event.conn.path_array = path_array;

	// checkin event with default dispatcher
	SR.EventManager.checkin(event);
});

LOG.sys('Auto-generated flow api', 'Flexflow');
